<?php

namespace LevelsRanks;

use LevelsRanks\Exception\CacheCreateFail;

class Config
{
    /**
     * Текущая версия
     *
     * @var string version
     */

    /**
     * Записей на странице
     *
     * @var int recordOnPage
     */

    /**
     * Какие использовать Звания
     * 18 - Старые значки от CS:GO
     * 19 - от levelsRanks
     * 20 - Обновленные значки от CS:GO
     * 21 - Значки из режима напарники
     *
     * @var int icon_id
     */

    /**
     * подпись (картинка) для форумов и т.п.
     * TRUE - включена, FALSE - выключена
     *
     * @var bool signature
     */

    /**
     * В списке игроков показывать
     * true  - количество очков
     * false - иконки рангов
     *
     * @var bool displayScore
     */

    /**
     * Показывать в профиле сервера на который статистика по данному игроку не найдена
     * true  - показывать
     * false - не показывать
     *
     * @var bool displayFreeServer
     */

    /**
     * Статистика STEAM
     * TRUE  - включена
     * FALSE - выключена
     *
     * @var bool steamStats
     */

    /**
     * Формат вывода КДА
     * true  - 2,3
     * false - 230%
     *
     * @var bool formatKDA
     */

    /**
     * Показывать в списке игроков и в профиле заблокирован игрок
     * Для работы необходимо подключене к SB
     * true - да
     * false - не показывать
     *
     * @var bool displayBan
     */

    /**
     * Показывать в списке игроков и в профиле замучен игрок
     * Для работы необходимо подключене к SB
     * true - да
     * false - не показывать
     *
     * @var bool displayMute
     */


    /**
     * Хранить информацию о аватарках
     * @var bool cacheSteamAva
     */

    /**
     * Сколько хранить информацию о аватарках
     * @var int cacheSteamAvaTime
     */

    /**
     * Кэширование аватарок стима
     * true  - включен
     * false - выключен
     *
     * @var bool CACHE
     */

    /**
     * Задаем время кеширования аватарок стима в секундах
     * 60 - минута
     * 3600 - час
     * 86400 - сутки
     *
     * @var int CACHETIME
     */

    /**
     * Кэширование сигнатур
     * true  - включен
     * false - выключен
     *
     * @var bool CACHE_SIG
     */

    /**
     * задаем время жизни картинок в секундах
     * 60 - минута
     * 3600 - час
     * 86400 - сутки
     *
     * @var int CACHETIME_SIG
     */

    /**
     * @var array
     */
    private $configs = [
        'system' => 2430,  // системная информация необходимая для определения версии вашего LR
        'version' => '2.4.3  for SB MaterialAdmin',
        'recordOnPage' =>  50,
        'icon_id' => 20,
        'signature' => false,
        'displayScore' => false,
        'displayFreeServer' => false,
        'steamStats' => true,
        'formatKDA' => true,
        'resetRank' => true,
        'typeStats' => false,
        'formatDate' => true,
        'cacheSteamAva' => true,
        'cacheSteamAvaTime' => 604800,
        'CACHE' => false,
        'CACHETIME' => 86400,
        'CACHE_SIG' => true,
        'CACHETIME_SIG' => 3600,
    ];


    public function __construct()
    {
        $this->getConfigs();
    }

    private function getConfigs()
    {
        if ( file_exists(__DIR__.'/Cache/config.php') ) {
            $this->configs = array_merge($this->configs, include __DIR__.'/Cache/config.php');
        }
    }

    /**
     * @return $this
     * @throws CacheCreateFail
     */
    public function save()
    {
        $string = "<?php\n return ".var_export($this->configs, true).';';
        if ( file_put_contents(__DIR__.'/Cache/config.php', $string) === false )
            throw new CacheCreateFail("Ошибка: не удается создать кеш конфигов");
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     * @throws \Exception
     */
    public function __set( $name, $value )
    {
        if ( isset($this->configs[$name]) ){
            if ( is_int($this->configs[$name]) )
                $value = (int)$value;
            elseif ( is_bool($this->configs[$name]) )
                $value = (bool)$value;
            $this->configs[$name] = $value;
        } else
            throw new \Exception("Переменая $name не найдена");
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (isset($this->configs[$name])) {
            return $this->configs[$name];
        } else
            throw new \Exception("Переменая $name не найдена");
    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function add($name, $value)
    {
        if ( !isset($this->configs[$name]) )
            $this->configs[$name] = $value;
    }

    public function has($name)
    {
        return isset($this->configs[$name]);
    }
}
