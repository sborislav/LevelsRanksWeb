<?php

namespace LevelsRanks\Entity;

class SteamPlayer
{
    public $avatar;

    public $avatarmedium;

    public $avatarfull;

    public $profileurl;

    public $personaname;

    public $personastate;

    public $realname;

    public $loccountrycode;

    public $lastlogoff;

    public $timecreated;

    public $gameid;

}