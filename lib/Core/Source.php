<?php

namespace LevelsRanks\Core;

use LevelsRanks\Avatars;

class Source
{
    /* @var $connect Connect[] */
    static protected $connect;

    /**
     * @var Avatars
     */
    static protected $avatars;
}