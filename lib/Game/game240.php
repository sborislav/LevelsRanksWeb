<?php

namespace LevelsRanks\Game;

class game240 extends Game
{
    protected $id = 240;

    protected $name = 'Counter-Strike: Source';
    protected $smallname = 'css';

    protected $array = [
        'i_Number_Of_Kills'         => 'kill',
        'i_Number_Of_Deaths'        => 'death',
        'i_Time_Played'             => 'time',
        'i_Number_Of_PlantedBombs'  => 'bombPlanted',
        'i_Number_Of_DefusedBombs'  => 'bombDefused',
        'total_wins'                => 'win',
        'i_Damage_Done'             => 'damage',
        'i_Money_Earned'            => 'money',
        'total_dominations'         => 'domination',
        'total_revenges'            => 'revenge',
        'i_NumShotsHit'             => 'hit',
        'i_NumShotsFired'           => 'shot',
        'total_kills_headshot'      => 'headshot',
        'total_kills_enemy_blinded' => 'killsBlinded',
        'total_kills_knife_fight'   => 'winsKnifeFight',
        'total_kills_enemy_weapon'  => 'killsEnemyWeapons',
        'total_wins_pistolround'    => 'winPistolRound',
    ];




}